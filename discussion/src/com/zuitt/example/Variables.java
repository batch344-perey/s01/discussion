package com.zuitt.example;
//A "package" in Java is used to group related classes. Think of it as a folder in a file directory
    //Packages are divided into 2 categories:
        //1. Built-in Packages (Packages from the Java API)
        //2. User-defined Packages (created by the user)
//Package creation in Java follows the "reverse domain name notation" for tne naming convention
public class Variables {
    public static void main (String[] args){
        //Naming Convention
            //the terminology used variable name is identifier.
            //All identifiers should begin with a letter (A to Z, a to z), currency ($) or an underscore (_) [SHOULD NOT BEGIN WITH A NUMBER]
            //After the first character, identifiers can have any combination of characters including numbers.
            //Most importantly, identifiers are case sensitive.

        //Variable declaration:
        int age;
        char middleName;

        //Variable declaration together with initialization:
        int x;
        int y = 0;

        //Initialization after declaration:
        x = 1;

        //output to the system:
        System.out.println("The value of y is " + y + " and the value of x is " + x);


        //Primitive Data Types:
            //predefined within the Java Programming Language which is used for single-valued variables with limited capabilities.

        // int - whole number values (integer)
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        //long - L is added to the end of the long number to be recognized;
        long worldPopulation = 12345567890L;
        System.out.println(worldPopulation);

        //Floating values:
            //1. float - add f at the end of the value
            //It can hold up to 7 decimal places
        float piFloat = 3.14159265359f;
        System.out.println("The value of piFloat is " + piFloat);

            //2. double - floating values
        double piDouble = 3.14159265359;
        System.out.println("The value of piDouble is " + piDouble);

        //char - single character only
        //      - uses single quote. (but for string should be double quote)
        char letter = 'a';
        System.out.println(letter);

        //boolean - true or false value
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        //constants
        //Java uses the "final" keyword so that variable's value cannot be changed.
        //to easily identify that the value is constant or final, the naming convention is set to UPPERCASE
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);


        //Non-primitive Data
            //also known as reference data types, refers to instances or objects.
            //doest not directly store the value of a variable but rather remember the reference to that variable.
        //String
        //Stores a sequence or array of characters
        //Strings are actually objects that can use methods.

        String username = "Jsmith";
        System.out.println(username);

        //Sample string method
        int stringLength = username.length();
        System.out.println(stringLength);

    }
}

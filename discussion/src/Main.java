import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    // public - access modifier which simply tells the application which classes have access to this method/attribute
    // static - keyword associated with method/property that is related in a class. This will allow a method to be invoked without instantiating a class
    // void - a keyword that is use to specify a method that doesn't return any value. In Java, we have to declare the data type of the method's return.
    //String[] args - accepts single argument of type string array that contains command line arguments
    public static void main(String[] args) {

        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;
        double gradeAve;


        Scanner myScanner = new Scanner(System.in);

        System.out.println("First Name: ");
        firstName = myScanner.nextLine();

        System.out.println("Last Name: ");
        lastName = myScanner.nextLine();

        System.out.println("First Subject Grade: ");
        firstSubject = myScanner.nextDouble();

        System.out.println("Second Subject Grade: ");
        secondSubject = myScanner.nextDouble();

        System.out.println("Third Subject Grade: ");
        thirdSubject = myScanner.nextDouble();

        gradeAve = ((firstSubject + secondSubject + thirdSubject)/3);

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + gradeAve);

    }
}

